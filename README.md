# cryptolib

A python library to keep track of your cryptocurrency, organise transactions, keep balances, calculate taxes etc

An example to show the usage is provided rather than extensive documentation for now. The library can easily be extended
with your own functionality as shown in the example. Any merge requests or suggestions are welcome.

Last used refers to last tax year I made use of it. So 2022 means I used it to generate tax report in 2023.

Data export possible to do with CoinGecko, see https://www.coingecko.com/en/api/pricing

## Export notes

Below my notes on how to get the correct table exported from each exchange supported. Always need a CSV or Excel file type export.

### Kraken

Go to History, Export, Select `Ledgers` and the time period.

Kraken's export table is quite a challenge to read, it typically requires some manual intervention. There are two lines per transaction for most transactions, but the two do not always come in order (e.g. when there was multiple deposits in short succession). The script expects that transactions that have two lines (two refid) comes after each other.

Last used: 2023

### Coinbase Standard

Click on Profile icon an select 'Statements'. In the 'Generate custom statement' box, select the Date range (for me, normally I select the last year I am currently doing report for), and select CSV.

This report has some jibberish at the top, please remove.

Then it contains unfortunately all transactions and not ordered by date for some reason. You might want to edit it to only contain the data from a given range (I do).

Finally, this CSV does not contain the correct amount on both side of advanced crypto trades, see e.g. https://help.coinledger.io/en/articles/2535264-coinbase-file-import-guide where they complain about the same. You need to manually EDIT these later

Note: Do not select Taxes at this stage. The specific file that is compatible with our software is the one under Transaction History.

Last used: 2023

#### Coinbase Pro (old)

Click on account profile image, select Statements, click on Generate - Account. Make sure to set CSV format, and select time range.

Last used: 2022

### Binance

NOT WORKING AT THE MOMENT, BASED ON OLD STYLE.


Binance was the worst of them all, now it is pretty much easiest of them all.

To download, go to 'Wallet' and 'Transaction History', and select 'Generate all statements'. By default all accounts and all coins are selected. Set the time period (can be an entire year now) and click 'Create'. This provides a csv file (inside a tar.gz archive) with all data needed.

Note: Only used spot trading, for other types you probably need to modify library.

Last used: 2021

### Bitstamp

Last used: 2019
### Kucoin

Last used: 2020

## Disclaimer

This is based on a "best effort basis". The tax calculation may be wrong, and it is the users responsibility to
report their taxes correctly. The code seems to calculate correctly for me, but other than that there has been
no verification of this code.

## TODO

 - FIFO, LIFO type of tax calculation
 - Verify correctness of calculations (in particular imports)
 - Mining fully ignored for now (I don't)

## Other similar tools

https://koinly.io

This looks more reasonably priced than cointracking, and seems to support export of K4 for Sweden. CSV imports are way less painful than with my tool.
I did note they do not import Coinbase advanced crypto trades correctly from CSV.

https://divly.com

Similar feedback as Koinly. Does not focus as much on CSV import perhaps, API import from e.g. Coinbase and Kraken tested and seems to work well.

https://coinledger.io

Not tried

https://github.com/bitsofwinter/cryptotaxsweden

(if I were you, I would probably rather use this tool for now..)

For Swedes, this forum page might be of interest: https://www.sweclockers.com/forum/trad/1512277-kryptovalutor-deklaration

Norwegians may find this tool useful https://www.kryptosekken.no/ I have not looked into the rules in Norway in any particular detail yet.
