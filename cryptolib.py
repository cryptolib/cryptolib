import csv
import time
import datetime
import dateutil
import os
import sys
from scipy import interpolate
import pandas as pd

"""
cryptolib
Copyright (C) 2019  cryptolib at gitlab.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have receiv0ed a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""


class cryptolib:
    """
    An object that holds all transactions,
    can write to CSV and can read in content from
    different exchanges..
    """

    def __init__(self, filename=None):
        self._list_of_transactions = []
        self._filename = filename
        self._converts = {"NANO": "XNO"}
        self._fiat_currencies = ["USD", "EUR", "GBP"]
        self._crypto_base = "USD"
        self._fiat_base = "CAD"
        self._valid_transaction_types = ["Withdrawal", "Deposit", "Trade", "Airdrop", "Stolen", "Taxes"]
        if filename is not None and os.path.isfile(filename):
            self.read_cointracking(filename)

    def set_fiat_base(self, base):
        """
        FIAT base is the currency you want taxes calculated in

        If different from crypto base, any crypto price is calculated
        first in crypto base, and then from there converted to fiat base.
        """
        self._fiat_base = base

    def set_crypto_base(self, base):
        """
        FIAT currency for calculating the value of crypto

        If different from crypto base, any crypto price is calculated
        first in crypto base, and then from there converted to fiat base.
        """
        self._crypto_base = base

    def write_csv(self, filename=None, date_start=None, date_end=None):
        """
        TODO: only write transaction within a date interval.

        :param filename: Name of csv file to write to.
        :param date_start: Not implemented
        :param date_end: Not implemented
        """
        if filename is not None:
            self._filename = filename

        with open(self._filename, "w") as fout:
            fieldnames = ["Type", "Buy", "BuyCurr", "Sell", "SellCurr", "Fee", "FeeCurr", "Exchange", "Group", "Comment", "Date"]
            writer = csv.DictWriter(fout, fieldnames=fieldnames)
            writer.writeheader()

            self._list_of_transactions.sort(key=lambda x: x["Date"])

            # All columns..
            for transaction in self._list_of_transactions:
                writer.writerow(transaction)

    def set_price_dictionary(self, price_dictionary):
        """
        Set a price dictionary for the crypto base

        Necessary to calculate taxes
        Between each date where a price is provided, a linear interpolation is used.
        """
        self._prices = price_dictionary

    def add_fiat_currency(self, fiat_currency):
        """
        Add a currency that should be recognized as FIAT.
        """
        if fiat_currency not in self._fiat_currencies:
            self._fiat_currencies.append(fiat_currency)

    def add_exchange_trade(
        self, timestamp, from_curr, from_amount, to_curr, to_amount, exchange="", fee=0.0, fee_curr="", type_="Trade", group="", comment=""
    ):
        transaction = {
            "Type": type_,
            "Buy": to_amount,
            "BuyCurr": to_curr,
            "Sell": from_amount,
            "SellCurr": from_curr,
            "Fee": fee,
            "FeeCurr": fee_curr,
            "Exchange": exchange,
            "Group": group,
            "Comment": comment,
            "Date": timestamp,
        }
        if type_ not in self._valid_transaction_types:
            print(transaction)
            raise ValueError("Type {} is not a recognized type of transaction".format(type_))
        self._list_of_transactions.append(transaction)

    def read_bitstamp(self, filename):
        """
        Read in transactions from a bitstamp export
        """
        print("Reading BitStamp file {}".format(filename))

        # The date format in the csv:
        dateformat = "%b. %d, %Y, %I:%M %p"
        # The possible fiat types on BitStamp
        fiats = ["USD", "EUR"]

        with open(filename, newline="") as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                if row["Type"] in ["Market"]:
                    if row["Sub Type"] == "Buy":
                        f = row["Value"]
                        t = row["Amount"]
                    elif row["Sub Type"] == "Sell":
                        t = row["Value"]
                        f = row["Amount"]

                    from_amount = float(f.split()[0])
                    from_curr = f.split()[1]
                    to_amount = float(t.split()[0])
                    to_curr = t.split()[1]

                    if from_curr in fiats or to_curr in fiats:
                        group = "FIAT"
                    else:
                        group = "Crypto"

                    tstamp = time.mktime(datetime.datetime.strptime(row["Datetime"], dateformat).timetuple())
                    self.add_exchange_trade(
                        exchange="BitStamp",
                        timestamp=tstamp,
                        from_curr=from_curr,
                        to_curr=to_curr,
                        from_amount=from_amount,
                        to_amount=to_amount,
                        fee=float(row["Fee"].split()[0]),
                        fee_curr=row["Fee"].split()[1],
                        group=group,
                    )
                elif sys.flags.debug:
                    print("Ignored", row)

    def read_binance_wd(self, filename, type_):
        """
        Read in deposits and withdrawals from a Binance style xlsx

        type_ is Withdrawal or Deposit
        """
        print("Reading Binance file {}".format(filename))

        dfs = pd.read_excel(filename, sheet_name="sheet1")

        coins = dfs["Coin"]

        # The date format in the Excel table
        dateformat = "%Y-%m-%d %H:%M:%S"

        for i in range(len(coins)):
            coin = dfs["Coin"][i]
            tstamp = time.mktime(datetime.datetime.strptime(dfs["Date(UTC)"][i], dateformat).timetuple())

            amount = dfs["Amount"][i]
            fee = float(dfs["TransactionFee"][i])

            comment = ", ".join([dfs["Address"][i], dfs["TXID"][i], str(dfs["SourceAddress"][i]), str(dfs["PaymentID"][i])])
            if type_ == "Withdrawal":
                from_amount = amount
                from_curr = coin
                to_amount = ""
                to_curr = ""
            elif type_ == "Deposit":
                to_amount = amount
                to_curr = coin
                from_amount = ""
                from_curr = ""
            else:
                raise ValueError("Wrong type_")
            self.add_exchange_trade(
                exchange="Binance",
                timestamp=tstamp,
                from_curr=from_curr,
                to_curr=to_curr,
                from_amount=from_amount,
                to_amount=to_amount,
                fee=fee,
                fee_curr=coin,
                type_=type_,
                comment=comment,
            )

    def read_binance(self, filename):
        """
        Read in transactions from a Binance style xlsx
        """
        print("Reading Binance file {}".format(filename))

        dfs = pd.read_excel(filename, sheet_name="sheet1")

        market = dfs["Market"]

        # The possible base pairs on Binance
        bases = ["BTC", "ETH", "BNB", "USDT", "EUR"]
        # The date format in the Excel table
        dateformat = "%Y-%m-%d %H:%M:%S"

        for i in range(len(market)):
            base = ""
            for b in bases:
                if market[i][-len(b) :] == b:
                    base = b
            if base == "":
                raise ValueError("Failed to recognize Binance market {}".format(market[i]))
            curr = market[i][: -len(base)]

            curr_amount = dfs["Amount"][i]
            base_amount = dfs["Total"][i]

            tstamp = time.mktime(datetime.datetime.strptime(dfs["Date(UTC)"][i], dateformat).timetuple())

            if dfs["Type"][i] == "BUY":
                self.add_exchange_trade(
                    exchange="Binance",
                    timestamp=tstamp,
                    from_curr=base,
                    to_curr=curr,
                    from_amount=base_amount,
                    to_amount=curr_amount,
                    fee=dfs["Fee"][i],
                    fee_curr=dfs["Fee Coin"][i],
                    group="Crypto",
                )
            elif dfs["Type"][i] == "SELL":
                self.add_exchange_trade(
                    exchange="Binance",
                    timestamp=tstamp,
                    from_curr=curr,
                    to_curr=base,
                    from_amount=curr_amount,
                    to_amount=base_amount,
                    fee=dfs["Fee"][i],
                    fee_curr=dfs["Fee Coin"][i],
                    group="Crypto",
                )
            else:
                raise ValueError("Failed to understand Binance type {}".format(dfs["Type"][i]))

    def read_coinbase(self, filename):
        """
        Read in transactions from a Coinbase export

        TODO: The new coinbase format is painful to use..
        TODO: Advanced crypto trades cannot be imported correcly at the moment??
        """

        print(f"Reading Coinbase file {filename}")

        # The date format in the csv:
        dateformat = "%Y-%m-%d %H:%M:%S UTC"

        with open(filename, newline="") as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                comment = row["Notes"]
                timestamp = time.mktime(datetime.datetime.strptime(row["Timestamp"], dateformat).timetuple())
                type_ = row["Transaction Type"]
                asset = row["Asset"]
                amount = float(row["Quantity Transacted"])
                nspl = row["Notes"].split()  # Seems we need some info from the notes column, ugh
                if type_ == "Withdrawal":
                    type_ = "Withdrawal"
                    from_amount = amount
                    from_curr = asset
                    to_amount = ""
                    to_curr = ""
                    fee = 0.0
                    fee_curr = ""
                    if from_curr in self._fiat_currencies:
                        group = "FIAT"
                    else:
                        group = "Crypto"
                elif type_ == "Receive":
                    type_ = "Deposit"
                    to_amount = amount
                    to_curr = asset
                    from_amount = ""
                    from_curr = ""
                    fee = 0.0
                    fee_curr = ""
                    if to_curr in self._fiat_currencies:
                        group = "FIAT"
                    else:
                        group = "Crypto"
                    fee = 0.0
                elif type_ == "Buy":
                    type_ = "Trade"
                    fee = float(row["Fees and/or Spread"])
                    fee_curr = ""
                    from_curr = nspl[-1]
                    from_amount = float(nspl[-2][1:])
                    to_curr = asset
                    to_amount = amount

                    group = "FIAT"
                elif type_ == "Advance Trade Buy":
                    type_ = "Trade"
                    to_amount = amount
                    to_curr = asset
                    from_amount = float(nspl[-4])
                    from_curr = nspl[-3]
                    fee = float(row["Fees and/or Spread"])
                    fee_curr = from_curr
                    if from_curr not in self._fiat_currencies:
                        comment += " EDIT NEEDED"
                        to_amount = None
                        fee = None
                elif type_ == "Advance Trade Sell":
                    type_ = "Trade"
                    from_amount = amount
                    from_curr = asset
                    to_amount = float(nspl[-4])
                    to_curr = nspl[-3]
                    fee = -1*float(row["Fees and/or Spread"])
                    fee_curr = to_curr
                    if to_curr not in self._fiat_currencies:
                        comment += " EDIT NEEDED"
                        to_amount = None
                        fee = None
                elif type_ == "Convert":
                    type_ = "Trade"
                    from_amount = amount
                    from_curr = nspl[-4]
                    to_amount = float(nspl[-2])
                    to_curr = nspl[-1]
                    fee = 0.0
                    fee_curr = ""
                else:
                    raise TypeError(f"Unknown transaction type '{type_}' for Coinbase")

                self.add_exchange_trade(
                    exchange="Coinbase",
                    type_=type_,
                    timestamp=timestamp,
                    from_curr=from_curr,
                    to_curr=to_curr,
                    from_amount=from_amount,
                    to_amount=to_amount,
                    fee=fee,
                    fee_curr=fee_curr,
                    group=group,
                    comment=comment,
                )

    def read_coinbasepro(self, filename):
        """
        Read in transactions from a Coinbase export

        TODO: This function has not been extensively tested.
        """

        print(f"Reading Coinbase file {filename}")

        # The date format in the csv:
        dateformat = "%Y-%m-%dT%H:%M:%S.%fZ"

        with open(filename, newline="") as csvfile:
            reader = csv.DictReader(csvfile)
            matchline = 0
            for row in reader:
                comment = f"{row['portfolio']}-{row['transfer id']}i-{row['trade id']}-{row['order id']}"
                timestamp = time.mktime(datetime.datetime.strptime(row["time"], dateformat).timetuple())
                type_ = row["type"][0].upper() + row["type"][1:]
                asset = row["amount/balance unit"]
                amount = float(row["amount"])
                if type_ == "Withdrawal":
                    from_amount = -amount
                    from_curr = asset
                    to_amount = ""
                    to_curr = ""
                    fee = 0.0
                    fee_curr = ""
                    group = "FIAT"
                elif type_ == "Deposit":
                    to_amount = amount
                    to_curr = asset
                    from_amount = ""
                    from_curr = ""
                    fee = 0.0
                    fee_curr = ""
                    group = "FIAT"
                    fee = 0.0
                elif type_ in ["Match", "Fee"]:
                    group = "Crypto"
                    if type_ == "Match":
                        if amount < 0:
                            from_curr = asset
                            from_amount = -amount
                        else:
                            to_curr = asset
                            to_amount = amount
                    elif type_ == "Fee":
                        fee_curr = asset
                        fee = -amount
                    type_ = "Trade"
                    # Read three lines before writing:
                    matchline = (matchline + 1) % 3

                if not matchline:
                    self.add_exchange_trade(
                        exchange="Coinbase",
                        type_=type_,
                        timestamp=timestamp,
                        from_curr=from_curr,
                        to_curr=to_curr,
                        from_amount=from_amount,
                        to_amount=to_amount,
                        fee=fee,
                        fee_curr=fee_curr,
                        group=group,
                        comment=comment,
                    )

    def read_kraken(self, filename):
        """
        Read in transactions from a Kraken export

        TODO: This function has not been extensively tested.
        TODO: Kraken sometimes adds some rounding entries, which
              we should just ignore. At the moment it corrupts everything
        """

        print(f"Reading Kraken file {filename}")

        # The date format in the csv:
        dateformat = "%Y-%m-%d %H:%M:%S"

        asset_dict = dict(
            XXBT="BTC",
            XETH="ETH",
            XLTC="LTC",
            XXMR="XMR",
            ZEUR="EUR",
        )
        with open(filename, newline="") as csvfile:
            reader = csv.DictReader(csvfile)
            first_tradeline = False
            for row in reader:
                comment = f"refid: {row['refid']}"
                timestamp = time.mktime(datetime.datetime.strptime(row["time"], dateformat).timetuple())
                type_ = row["type"][0].upper() + row["type"][1:]
                asset = row["asset"]
                if asset in asset_dict:
                    asset = asset_dict[asset]
                amount = float(row["amount"])
                if type_ in ["Deposit", "Withdrawal"] and not row["txid"]:
                    continue
                first_tradeline = (not first_tradeline) and (type_ in ["Trade"])
                if type_ == "Withdrawal":
                    fee = float(row["fee"])
                    fee_curr = asset
                    from_amount = amount
                    from_curr = asset
                    to_amount = ""
                    to_curr = ""
                    group = "FIAT"
                    refid = row["refid"]
                elif type_ == "Deposit":
                    fee = float(row["fee"])
                    fee_curr = asset
                    to_amount = amount
                    to_curr = asset
                    from_amount = ""
                    from_curr = ""
                    group = "FIAT"
                    refid = row["refid"]
                else:  # Trade..
                    group = "Crypto"
                    if amount < 0:
                        from_curr = asset
                        from_amount = -amount
                    else:
                        to_curr = asset
                        to_amount = amount
                    fee_ = float(row["fee"])
                    if fee_:
                        fee_curr = asset
                        fee = fee_
                if first_tradeline:
                    refid = row["refid"]
                else:
                    if row["refid"] != refid:
                        print(row)
                        print("Previous refid:", refid)
                        raise ValueError(f"Failing to read line in Kraken file {filename}")
                    self.add_exchange_trade(
                        exchange="Kraken",
                        type_=type_,
                        timestamp=timestamp,
                        from_curr=from_curr,
                        to_curr=to_curr,
                        from_amount=from_amount,
                        to_amount=to_amount,
                        fee=fee,
                        fee_curr=fee_curr,
                        group=group,
                        comment=comment,
                    )

    def read_kucoin(self, filename):
        """
        Read in transactions from a Kucoin style CSV
        """
        print("Reading Kucoin file {}".format(filename))

        # The date format in the csv:
        dateformat = "%Y-%m-%d %H:%M:%S.0"

        with open(filename, newline="") as csvfile:
            reader = csv.DictReader(csvfile)

            for row in reader:
                tstamp = time.mktime(datetime.datetime.strptime(row["createdDate"], dateformat).timetuple())

                if row["direction"] == "BUY":
                    from_curr = row["symbol"].split("-")[1]
                    to_curr = row["symbol"].split("-")[0]
                    from_amount = float(row["dealValue"])
                    to_amount = float(row["amount"])
                else:
                    from_curr = row["symbol"].split("-")[0]
                    to_curr = row["symbol"].split("-")[1]
                    from_amount = float(row["amount"])
                    to_amount = float(row["dealValue"])
                fee = float(row["fee"])
                fee_curr = row["symbol"].split("-")[1]

                self.add_exchange_trade(
                    exchange="Kucoin",
                    timestamp=tstamp,
                    from_curr=from_curr,
                    to_curr=to_curr,
                    from_amount=from_amount,
                    to_amount=to_amount,
                    fee=fee,
                    fee_curr=fee_curr,
                    group="Crypto",
                    comment=row["oid"],
                )

    def read_kucoin_wd(self, filename, type_):
        """
        Read in deposits/withdrawals from a Kucoin style CSV

        :param _type: Deposit or Withdrawal
        """
        print("Reading Kucoin {} file {}".format(type_, filename))

        # The date format in the csv:
        dateformat = "%Y-%m-%d %H:%M:%S"

        with open(filename, newline="") as csvfile:
            reader = csv.DictReader(csvfile)

            for row in reader:
                tstamp = time.mktime(datetime.datetime.strptime(row["Time"], dateformat).timetuple())

                if type_ == "Deposit":
                    to_curr = row["Coin"]
                    to_amount = float(row["Amount"])
                    from_amount = ""
                    from_curr = ""
                elif type_ == "Withdrawal":
                    from_curr = row["Coin"]
                    from_amount = float(row["Amount"])
                    to_amount = ""
                    to_curr = ""
                else:
                    raise ValueError("Not yet implemented")
                comment = "{} {}".format(row["Type"], row["Remark"])

                self.add_exchange_trade(
                    exchange="Kucoin",
                    timestamp=tstamp,
                    from_curr=from_curr,
                    to_curr=to_curr,
                    from_amount=from_amount,
                    to_amount=to_amount,
                    fee="",
                    fee_curr="",
                    group="Crypto",
                    type_=type_,
                    comment=comment,
                )

    def read_lbc(self, filename, username):
        """
        Read in transactions from a LocalBitcoins style CSV
        """
        print("Reading LocalBitcoin file {}".format(filename))

        # The date format in the csv:
        dateformat = "%Y-%m-%d %H:%M:%S+00:00"

        with open(filename, newline="") as csvfile:
            reader = csv.DictReader(csvfile)

            for row in reader:
                tstamp = time.mktime(datetime.datetime.strptime(row["transaction_released_at"], dateformat).timetuple())

                fiat_amount = float(row["fiat_amount"]) - float(row["fiat_fee"])
                btc_amount = float(row["btc_amount_less_fee"])
                # Making a comment to be able to track back transaction..
                comment = "{}, {} - {}".format(row["reference"], row["buyer"], row["seller"])
                if row["buyer"] == username:
                    self.add_exchange_trade(
                        exchange="LocalBitCoins",
                        timestamp=tstamp,
                        from_curr=row["currency"],
                        to_curr="BTC",
                        from_amount=fiat_amount,
                        to_amount=btc_amount,
                        fee=float(row["fiat_fee"]),
                        fee_curr=row["currency"],
                        group="FIAT",
                        comment=comment,
                    )
                elif row["seller"] == username:
                    self.add_exchange_trade(
                        exchange="LocalBitCoins",
                        timestamp=tstamp,
                        from_curr="BTC",
                        to_curr=row["currency"],
                        from_amount=btc_amount,
                        to_amount=fiat_amount,
                        fee=float(row["fee_btc"]),
                        fee_curr="BTC",
                        group="FIAT",
                        comment=comment,
                    )
                else:
                    raise ValueError("Failed to process LBC transaction", row)

    def read_cointracking(self, filename, datefmt="%Y-%m-%d %H:%M:%S"):
        """
        Reads a CSV export from CoinTracking

        This follows exactly the same format as we do, except for the headers,

        And we use timestamp rather than a date string

        We also insert numbers for Fee, Buy, Sell rather than an empty string
        """
        print("Reading CoinTracking file {}".format(filename))

        with open(filename, newline="") as csvfile:
            # DictReader cannot be used for the CoinTracking format
            reader = csv.reader(csvfile)
            for row in reader:
                if row[0] == "Type":
                    continue

                for i in [1, 3, 5]:
                    if row[i] == "":
                        row[i] = 0.0
                    else:
                        row[i] = float(row[i])

                if "-" in row[10]:
                    tstamp = time.mktime(datetime.datetime.strptime(row[10], datefmt).timetuple())
                else:
                    tstamp = float(row[10])

                self.add_exchange_trade(
                    type_=row[0],
                    to_amount=row[1],
                    to_curr=row[2],
                    from_amount=row[3],
                    from_curr=row[4],
                    fee=row[5],
                    fee_curr=row[6],
                    exchange=row[7],
                    group=row[8],
                    comment=row[9],
                    timestamp=tstamp,
                )

    def convert_currency_ticker(self, from_ticker, to_ticker):
        """
        This can be used to clean up transaction list,
        if for example a different ticker was used in the past
        for the same currency.
        E.g. XRB -> NANO
        """
        for tr in self._list_of_transactions:
            for key in ["BuyCurr", "SellCurr", "FeeCurr"]:
                if tr[key] == from_ticker:
                    tr[key] = to_ticker

    def calculate_balance(self, date=None, include_fiat=False, exchange=None):
        """
        Calculate current balance. Alternatively, balance at a specified date.
        Can also calculate (negative) balance of FIAT

        Please note, deposit/withdrawal are ignored here unless you request
        from a specific exchange.
        That means that for total balances, d/w are assumed transactions
        between accounts you own.
        That also means, that for total balances the net FIAT you have deposited
        will show as negative balances for those currencies.

        :param date: Calculate the balance on this date (default today)
        :param include_fiat: Calculate balances for FIATs as well (default no)
        :param exchange: Only calculate balance on a specific exchange (default all)
        """
        balances = {}
        if date is not None:
            timetuple = time.mktime(date.timetuple())

        for tr in self._list_of_transactions:
            if tr["Type"] == "Taxes":
                continue
            # Unless exchange is specified, skip deposits/withdrawals
            if tr["Type"] in ["Deposit", "Withdrawal"] and exchange is None:
                continue
            if exchange and tr["Exchange"] != exchange:
                continue
            if date is None or timetuple > tr["Date"]:
                # Include Buy in the balance:
                if tr["Buy"]:
                    if include_fiat or tr["BuyCurr"] not in self._fiat_currencies:
                        if tr["BuyCurr"] not in balances:
                            balances[tr["BuyCurr"]] = tr["Buy"]
                        else:
                            balances[tr["BuyCurr"]] += tr["Buy"]

                # Include Sell in the balance:
                if tr["Sell"]:
                    if include_fiat or tr["SellCurr"] not in self._fiat_currencies:
                        if tr["SellCurr"] not in balances:
                            balances[tr["SellCurr"]] = -tr["Sell"]
                        else:
                            balances[tr["SellCurr"]] -= tr["Sell"]

                # Include Fee in the balance, also for deposit/withdrawals:
                if tr["Fee"] and (include_fiat or tr["FeeCurr"] not in self._fiat_currencies):
                    if tr["FeeCurr"] not in balances:
                        balances[tr["FeeCurr"]] = -tr["Fee"]
                    else:
                        balances[tr["FeeCurr"]] -= tr["Fee"]

        return balances

    def calculate_fees(self, exchange=None, convert_dict=None):
        """
        Return a dictionary with all fees paid
        Optionally only calculate fees for a specific exchange

        :param exchange: Name of exchange (will only look at trades here)
        :param convert_dict: Convert fees based on this dictionary
        """
        if convert_dict:
            fees = 0.0
            base = None
        else:
            fees = {}
        for t in self._list_of_transactions:
            if t["Fee"] > 0:
                if exchange is not None and t["Exchange"] != exchange:
                    continue
                if convert_dict:
                    if t["FeeCurr"] in convert_dict:
                        factor = convert_dict[t["FeeCurr"]].get_price_at(t["Date"])
                        fees += factor * t["Fee"]
                    elif base:
                        if base == t["FeeCurr"]:
                            fees += t["Fee"]
                        else:
                            raise ValueError(f"Thought {base} was base currency of dictionary, now also found {t['Fee']} could be")
                    else:
                        base = t["FeeCurr"]
                        fees += t["Fee"]

                else:
                    if t["FeeCurr"] not in fees:
                        fees[t["FeeCurr"]] = t["Fee"]
                    else:
                        fees[t["FeeCurr"]] += t["Fee"]
        return fees

    def _add_tr_to_list(self, transaction_list, currency, transaction):
        if currency not in transaction_list:
            transaction_list[currency] = []
        transaction_list[currency].append(transaction)

    def calculate_taxes(self, year, strategy="FIFO", only_fiat=False):
        """
        Strategies implemented: Swedish

        TODO: Strategies planned: FIFO, LIFO, HIFO, WA (weighted average)

        only_fiat is True if crypto-crypto is not a taxed event

        TODO: only_fiat True is not implemented yet
        """
        if strategy not in ["Swedish"]:
            raise ValueError("Strategy {} not yet implemented".format(strategy))

        # The Swedish model..
        # Calculate the sum of all sells of a currency, in SEK equivalent, during the year
        # Then, calculate the purchase price of that amount of currency, also in SEK
        # The difference is the taxable profit/loss to be reported.

        after_date = time.mktime(datetime.date(year, 1, 1).timetuple())
        before_date = time.mktime(datetime.date(year + 1, 1, 1).timetuple())

        # For each currency, a list of all buy transactions
        # Whenever a fee is paid in crypo, calculated as that amount being sold at 0 FIAT
        all_buys = {}
        all_sells = {}

        has_warned = False

        for tr in self._list_of_transactions:
            if tr["Date"] > before_date:
                continue
            if only_fiat and tr["BuyCurr"] not in self._fiat_currencies and tr["SellCurr"] not in self._fiat_currencies:
                if tr["FeeCurr"] in self._fiat_currencies:
                    print(tr)
                    raise TypeError("Not sure how to deal with this transaction yet")
                if sys.flags.debug:
                    print(f"Not including transaction in tax calculation: {tr}")
                continue
            if tr["Type"] in ["Trade", "Stolen"]:
                if tr["Sell"] == 0.0:  # this would mean an airdrop or similar
                    base_value = 0
                else:
                    if not tr["BuyCurr"]:
                        print(tr)
                    base_value = self._prices[tr["BuyCurr"]].get_price_at(tr["Date"]) * tr["Buy"]
                if self._crypto_base != self._fiat_base:
                    base_value /= self._prices[self._fiat_base].get_price_at(tr["Date"])

                if tr["FeeCurr"] in self._fiat_currencies:
                    if tr["FeeCurr"] == self._fiat_base:
                        base_value += tr["Fee"]
                    elif tr["FeeCurr"] == self._crypto_base:
                        base_value += tr["Fee"] / self._prices[self._fiat_base].get_price_at(tr["Date"])
                    else:
                        base_value += (
                            tr["Fee"]
                            * self._prices[tr["FeeCurr"]].get_price_at(tr["Date"])
                            / self._prices[self._fiat_base].get_price_at(tr["Date"])
                        )

                self._add_tr_to_list(all_buys, tr["BuyCurr"], [tr["Buy"], base_value])
                self._add_tr_to_list(all_sells, tr["SellCurr"], [tr["Sell"], base_value])

            if tr["Fee"] and (tr["FeeCurr"] not in self._fiat_currencies or tr["Type"] not in ["Trade", "Stolen"]):
                self._add_tr_to_list(all_sells, tr["FeeCurr"], [tr["Fee"], 0.0])

            if tr["Type"] == "Taxes":
                if tr["Date"] > after_date and not has_warned:
                    print("WARNING: You have taxes already accounted for this year?")
                    has_warned = True

                # In this case we make a "negative buy" for the amount of crypto already taxed,
                # and a "negative" sell for the amount of FIAT this was reported as bought for
                # Similarly, we negatively sell the crypto for a negative amount of FIAT
                # that it was reported as sold for last year.
                if tr["SellCurr"] in self._fiat_currencies:
                    self._add_tr_to_list(all_buys, tr["BuyCurr"], [-tr["Buy"], -tr["Sell"]])
                else:
                    self._add_tr_to_list(all_sells, tr["SellCurr"], [-tr["Sell"], -tr["Buy"]])

        return all_buys, all_sells

    def add_taxes(self, year, all_buys, all_sells):
        import numpy

        tstamp = time.mktime(datetime.date(year, 12, 31).timetuple())
        for k in all_sells:
            if k not in self._fiat_currencies:
                s_k = numpy.array(all_sells[k])
                amount = sum(s_k[:, 0])
                sell_sum = sum(s_k[:, 1])
                if k in all_buys:
                    b_k = numpy.array(all_buys[k])
                    amount_b = sum(b_k[:, 0])
                    buy_sum = sum(b_k[:, 1])
                    if amount_b > 0:
                        avg_buy = buy_sum / amount_b
                    else:
                        avg_buy = 0.0
                    cost = avg_buy * min(amount, amount_b)
                else:
                    cost = 0.0

                if amount != 0:
                    print("DBG", k, amount, sell_sum)
                else:
                    print("DBG2", k, amount, sell_sum)
                if amount > 0:  # Is that always correct?
                    # maybe rather amount = cost = sell_sum = 0 -> continue?
                    self.add_exchange_trade(
                        type_="Taxes",
                        timestamp=tstamp,
                        from_curr=self._fiat_base,
                        to_curr=k,
                        from_amount=cost,
                        to_amount=amount,
                        group="Reported Cost",
                    )
                    self.add_exchange_trade(
                        type_="Taxes",
                        timestamp=tstamp,
                        from_curr=k,
                        to_curr=self._fiat_base,
                        from_amount=amount,
                        to_amount=sell_sum,
                        group="Reported Sell",
                    )


class gecko_coins_list:
    def __init__(self):
        import requests

        url = "https://api.coingecko.com/api/v3/coins/list"
        resp = requests.get(url=url)
        self.data = resp.json()
        self.tickers = {}
        self.names = {}
        for d in self.data:
            if d["symbol"] in self.tickers:
                if isinstance(self.tickers[d["symbol"]], str):
                    self.tickers[d["symbol"]] = [self.tickers[d["symbol"]], d["id"]]
                else:
                    self.tickers[d["symbol"]].append(d["id"])
            else:
                self.tickers[d["symbol"]] = d["id"]
            self.names[d["id"]] = d["symbol"]

    def get_name_from_ticker(self, ticker):
        return self.tickers[ticker.lower()]

    def get_ticker_from_name(self, name):
        return self.names[name.lower()]

    def has_ticker(self, ticker):
        return ticker.lower() in self.tickers


class price:
    def __init__(self, ticker, base=""):
        """
        Set up a new price class for coin with ticker `coin'.

        :param ticker: the ticker for the coin
        :param base: the base currency (fiat)
        :param dates: list of dates (same length as prices)
        :param prices: list of prices (same length as dates)
        """
        self.ticker = ticker
        self.name = ""
        self.base = base
        self._dates = []
        self._prices = []
        self._fun = None

    def set_name(self, name):
        self.name = name

    def append_prices(self, dates, prices):
        self._dates.extend(dates)
        self._prices.extend(prices)

    def read_csv(self, fname, price_column, date_column, dateformat="%Y-%m-%d"):
        """
        Reads a CSV that must have a header column. Give the name of file,
        the name of column for price, and the name of column for dates.
        Date format should also be provided.
        """
        self._fun = None
        for row in csv.DictReader(open(fname, "r")):
            self._dates.append(datetime.datetime.strptime(row[date_column], dateformat))
            self._prices.append(float(row[price_column]))

    def get_gecko_data(self, year_or_dates, gecko_list):
        """
        year_or_dates: Either two datetime objects (start, end) or a year (int)
        gecko_list: 
        """
        import datetime
        import time
        import requests

        if not self.base:
            raise ValueError("Need to define base currency in order to use coingecko api")

        if isinstance(year_or_dates, int):
            dates = [
                datetime.datetime.fromordinal(ordinal)
                for ordinal in range(
                    datetime.date(year_or_dates, 1, 1).toordinal(),
                    datetime.date(year_or_dates + 1, 1, 1).toordinal(),
                )
            ]
        else:
            #TODO Maybe add a check here to verify correct format used
            dates = year_or_dates

        days_past = (datetime.datetime.now() - dates[0]).days
        if days_past > 365:
            raise ValueError(f"CoinGecko Public API only allows to go 365 days in the past, you tried to get from {dates[0]}")

        if self.name == "":
            name = gecko_list.get_name_from_ticker(self.ticker)
        else:
            name = self.name
        if not isinstance(name, str):
            raise TypeError(f"Multiple currencies for ticker {self.ticker}: {name}")

        u1 = int(time.mktime(dates[0].timetuple()))
        u2 = int(time.mktime(dates[-1].timetuple())) + 2 * 86400  # two extra days
        url = f"https://api.coingecko.com/api/v3/coins/{name}/market_chart/range?vs_currency={self.base.lower()}&from={u1}&to={u2}"
        resp = requests.get(url=url)
        data = resp.json()
        if "prices" not in data:
            print(f"Error getting '{url}'")
            if "error" in data:
                status = data["error"]["status"]
            elif "status" in data:
                status = data["status"]
            else:
                print(data)
                print("Unclear keys in error return object: {data.keys()}")
            print(status["error_code"], ":", status["error_message"])
            raise ValueError(f"Did not manage to download data for {self.name}")

        prices = [d[1] for d in data["prices"]]
        dates = [datetime.datetime.fromtimestamp(d[0] / 1000) for d in data["prices"]]

        self._dates.extend(dates)
        self._prices.extend(prices)

    def _recalculate_fit_function(self):
        # Sort by date first:
        self._prices = [x for _, x in sorted(zip(self._dates, self._prices))]
        self._dates.sort()

        # interpolation function doesn't use datetime directly:
        dates = [time.mktime(t.timetuple()) for t in self._dates]
        self._fun = interpolate.interp1d(dates, self._prices)

    def get_price_at(self, date):
        """
        Receives a datetime object, returns price at that date
        """
        if self._fun is None:
            self._recalculate_fit_function()

        try:
            price_at_date = self._fun(date)
        except ValueError:
            raise ValueError(f"Failed to find price for {self.ticker} at date {datetime.datetime.fromtimestamp(date)}")

        return price_at_date
