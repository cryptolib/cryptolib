import time
import datetime
from cryptolib import cryptolib, price
import numpy as np


# Need to add deposits and withdrawals (fees should be subtracted from totals)

mytrans = cryptolib()

mytrans.read_cointracking("btcx-transactions.csv")
mytrans.read_cointracking("Bitgrail-transactions.csv")
mytrans.read_cointracking("Coinbase-transactions.csv")

mytrans.read_binance("binance-TradeHistory_2017.xlsx")
mytrans.read_binance("binance-TradeHistory_2018.xlsx")
mytrans.read_binance_wd("binance_WithdrawalHistory.xlsx", "Withdrawal")
mytrans.read_binance_wd("binance_DepositHistory.xlsx", "Deposit")

mytrans.read_bitstamp("bitstamp_Transactions_2018.csv")

mytrans.read_lbc("lbc_contacts_finished_2018-06-25.csv", "yngvel")

mytrans.convert_currency_ticker("XRB", "NANO")
mytrans.convert_currency_ticker("VEN", "VET")

mytrans.add_fiat_currency("NOK")
mytrans.add_fiat_currency("SEK")

# Now we write all transactios to a CSV for backup..
mytrans.write_csv("2018_transactions_all.csv", date_start=datetime.date(2018, 1, 1), date_end=datetime.date(2018, 12, 31))


# Time to set up a price dictionary, needed to calculate taxes
# You can find your own way to get a list of exchange rates for each pair
# either manually inserting or through some sort of API if you have access to that
# E.g. from coinmarketcap, xe.com ...
# All transactions are first calculated to/from the crypto_base, and then from your crypto_base to fiat_base
# Here we assume you put all files in a subfolder prices/

usd_prices = {}

for fiat in ["SEK", "EUR", "NOK"]:
    usd_prices[fiat] = price(fiat)
    usd_prices[fiat].read_csv("prices/{}.csv".format(fiat.lower()), "Price(USD)", "Date")

for ticker in [
    "BAT",
    "BNB",
    "BTC",
    "ENG",
    "ETH",
    "LTC",
    "NANO",
    "NEO",
    "REQ",
    "VET",
    "XLM",
    "XMR",
    "OMG",
    "ICX",
    "IOTA",
    "WTC",
    "CND",
    "QTUM",
    "USDT",
    "GRLC",
]:
    usd_prices[ticker] = price(ticker)
    usd_prices[ticker].read_csv("prices/{}.csv".format(ticker.lower()), "Close", "Date")


mytrans.set_crypto_base("USD")
mytrans.set_fiat_base("SEK")

mytrans.set_price_dictionary(usd_prices)


# Now we can calculate the taxes. For now only the "Swedish model" is implemented, no idea if
# this logic has any formal name. The explanation can be found in Swedish here:
# https://skatteverket.se/privat/skatter/vardepapper/andratillgangar/kryptovalutor.4.15532c7b1442f256bae11b60.html
# It doesn't 100% follow this logic. Average buy/sell is calculated for each currency for the full
# year, while in reality any buy after a sell should not affect the avg. buy used to "compensate" that sell.
# This is having little impact on my calculations (and avoids some headache), but your mileage might differ.
# Any merge requests with better calculations are very welcome!

b, s = mytrans.calculate_taxes(2018, "Swedish", True)

tb = 0.0
ts = 0.0
tv = 0.0

remains = []
print("Valutakod\tAntal\t\tFörsäljningspris\tOmkostnadsbelopp\tVinst/Förlust")
for k in s:
    if k not in ["SEK", "USD", "EUR", "NOK"]:
        s_k = np.array(s[k])
        antal = sum(s_k[:, 0])
        avg_sell = sum(s_k[:, 1]) / antal
        ts += sum(s_k[:, 1])
        fs = sum(s_k[:, 1])
        if k in b:
            b_k = np.array(b[k])
            antal_buy = sum(b_k[:, 0])
            pris_buy = sum(b_k[:, 1])
            avg_buy = pris_buy / antal_buy
            omkostnad = avg_buy * min(antal, antal_buy)
            if sum(b_k[:, 1]) - sum(s_k[:, 1]) > 10:
                r = antal_buy - antal
                remains.append([k, r, avg_buy])
            vinst = fs - omkostnad
            tv += vinst
        else:
            avg_buy = 0.0
            omkostnad = 0.0
            pris_buy = 0.0
        tb += omkostnad
        print("{}\t\t{:.6f}\t{:.0f}\t\t\t{:.0f}\t\t\t{:.2f}".format(k, antal, fs, omkostnad, vinst))

print("\nTotal", tb, ts, tb - ts, tv)

print("\nIgenvarande medel")
tot_value = 0
for r in remains:
    tot_value += r[2] * r[1]
    if r[2] * r[1] > 10:
        print("{0:.4} {1} till {2:.2f} SEK/{1}, tot purchase {3:.2f} SEK".format(r[1], r[0], r[2], r[2] * r[1]))
print("Totalt omkostnadsbelopp:", tot_value)

# End of tax calculation example


# Another example (instead of tax calculation, show the balances today..)
# Calculate current balance as pie chart


from matplotlib import pyplot as plt

# % of holdings to show as separate piece, all below will go into "Others"
limit = 2.0
# The date you want to show balance for
date = datetime.date(2018, 12, 31)


# Calculation..
td = time.mktime(date.timetuple())

balances = mytrans.calculate_balance(date=date, include_fiat=False)
values = {}
percentages = {}
pieces = []
labels = []
others = 0
total = 0

for k in balances:
    if balances[k] > 0:
        values[k] = balances[k] * mytrans._prices[k].get_price_at(td)
        total += values[k]
for k in values:
    percentages[k] = values[k] * 100 / total

for k in balances:
    if balances[k] > 0:
        percentage = percentages[k]
        if percentage > limit:
            labels.append(k)
            pieces.append(percentage)
        else:
            others += percentage
if others > 0:
    labels.append("Other")
    pieces.append(others)

plt.figure(figsize=(8, 8))
plt.pie(pieces, labels=labels, autopct="%1.1f%%")
plt.show()

# End of balance plot example
